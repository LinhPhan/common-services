/**
 * Copyright 2015 Posiba, Inc. All rights reserved.
 * POSIBA, INC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF POSIBA, INC.
 * The copyright notice above does not evidence any actual or intended
 * publication of such source code.
 **/
package org.posiba.commons.services;

import java.io.File;
import java.io.IOException;

/**
 *
 * @author Linh Phan created on Jan 29, 2016 10:23:37 AM
 */
public interface ImageService {
    public File produceImageFromBase64(String imageName, String imageBase64) throws IOException;
}
