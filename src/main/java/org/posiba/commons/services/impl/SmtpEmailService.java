/**
 * Copyright 2015 Posiba, Inc. All rights reserved.
 * POSIBA, INC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF POSIBA, INC.
 * The copyright notice above does not evidence any actual or intended
 * publication of such source code.
 **/
package org.posiba.commons.services.impl;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.posiba.commons.services.EmailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Linh Phan created on Jan 28, 2016 11:46:15 AM
 */
public class SmtpEmailService extends EmailService {
    private String host;
    private String port;
    private String username;
    private String password;

    private List<String> environments;
    private String currentEnvironment;

    /**
     * @return the host
     */
    public String getHost() {
        return host;
    }

    /**
     * @param host
     *            the host to set
     */
    public void setHost(String host) {
        this.host = host;
    }

    /**
     * @return the port
     */
    public String getPort() {
        return port;
    }

    /**
     * @param port
     *            the port to set
     */
    public void setPort(String port) {
        this.port = port;
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username
     *            the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password
     *            the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the environments
     */
    public List<String> getEnvironments() {
        return environments;
    }

    /**
     * @param environments
     *            the environments to set
     */
    public void setEnvironments(List<String> environments) {
        this.environments = environments;
    }

    /**
     * @return the currentEnvironment
     */
    public String getCurrentEnvironment() {
        return currentEnvironment;
    }

    /**
     * @param currentEnvironment
     *            the currentEnvironment to set
     */
    public void setCurrentEnvironment(String currentEnvironment) {
        this.currentEnvironment = currentEnvironment;
    }

    private static final Logger logger = LoggerFactory.getLogger(SmtpEmailService.class);

    public void send(String host, String port, final String username, final String password, String sender, String recipient, String subject, String content, Map<String, File> attachments) {
        if (StringUtils.isEmpty(sender)) {
            logger.error("Email is NOT sent because Sender is empty.");
            return;
        }

        if (StringUtils.isEmpty(recipient)) {
            logger.error("Email is NOT sent because Recipient is empty.");
            return;
        }

        // if (StringUtils.isEmpty(content)) {
        // logger.error("Email is NOT sent from {} to {} because Content is empty.", sender, recipient);
        // return;
        // }

        if (CollectionUtils.isNotEmpty(environments) && environments.contains(currentEnvironment)) {
            return;
        }

        try {
            logger.info("Sending an email using Amazon SMTP... from {} to {}", sender, recipient);

            Properties props = new Properties();
            // props.put("mail.transport.protocol", "smtp");
            props.put("mail.smtp.host", host);
            props.put("mail.smtp.socketFactory.port", port);
            props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
            props.put("mail.smtp.auth", "true");
            // props.put("mail.smtp.ssl.enable", "false");

            Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(username, password);
                }
            });

            Message message = produceMessage(session, sender, recipient, subject, content, attachments);

            Transport.send(message);

            logger.info("Email has been sent from {} to {}", sender, recipient);
        } catch (Exception e) {
            logger.error("Email is NOT sent from {} to {} because of exception {}", sender, recipient, e.getMessage());
        }
    }

    public void send(String sender, String recipient, String subject, String content, Map<String, File> attachments) {
        if (StringUtils.isEmpty(sender)) {
            logger.error("Email is NOT sent because Sender is empty.");
            return;
        }

        if (StringUtils.isEmpty(recipient)) {
            logger.error("Email is NOT sent because Recipient is empty.");
            return;
        }

        // if (StringUtils.isEmpty(content)) {
        // logger.error("Email is NOT sent from {} to {} because Content is empty.", sender, recipient);
        // return;
        // }

        if (CollectionUtils.isNotEmpty(environments) && environments.contains(currentEnvironment)) {
            return;
        }

        try {
            logger.info("Attempting to send an email through Amazon SMTP... to {}", recipient);

            Properties props = new Properties();
            // props.put("mail.transport.protocol", "smtp");
            props.put("mail.smtp.host", host);
            props.put("mail.smtp.socketFactory.port", port);
            props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
            props.put("mail.smtp.auth", "true");
            // props.put("mail.smtp.ssl.enable", "false");

            Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(username, password);
                }
            });

            Message message = produceMessage(session, sender, recipient, subject, content, attachments);

            Transport.send(message);

            logger.info("Email has been sent from {} to {}", sender, recipient);
        } catch (Exception e) {
            logger.error("Email is NOT sent from {} to {} because of exception {}", sender, recipient, e.getMessage());
        }
    }

    private Message produceMessage(Session session, String sender, String recipient, String subject, String content, Map<String, File> attachments) throws AddressException, MessagingException {
        Message message = new MimeMessage(session);
        message.setHeader("Content-Type", "text/html");
        message.setFrom(new InternetAddress(sender));
        message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(recipient));
        message.setSubject(subject);

        if (MapUtils.isEmpty(attachments)) {
            message.setContent(content, "text/html; charset=UTF-8");
        } else {

            // Create the message part
            BodyPart messageBodyPart = new MimeBodyPart();

            // Now set the actual message
            messageBodyPart.setText(content);

            // Create a multipar message
            Multipart multipart = new MimeMultipart();

            // Set text message part
            multipart.addBodyPart(messageBodyPart);

            // Part two is attachment

            for (Entry<String, File> attachment : attachments.entrySet()) {
                messageBodyPart = new MimeBodyPart();
                DataSource source = new FileDataSource(attachment.getValue());
                messageBodyPart.setDataHandler(new DataHandler(source));
                messageBodyPart.setFileName(attachment.getKey());
                multipart.addBodyPart(messageBodyPart);
            }

            // Send the complete message parts
            message.setContent(multipart);
        }

        return message;
    }
}
