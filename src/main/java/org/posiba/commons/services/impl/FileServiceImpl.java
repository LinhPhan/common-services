/**
 * Copyright 2015 Posiba, Inc. All rights reserved.
 * POSIBA, INC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF POSIBA, INC.
 * The copyright notice above does not evidence any actual or intended
 * publication of such source code.
 **/
package org.posiba.commons.services.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.posiba.commons.services.FileService;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.PdfWriter;

/**
 *
 * @author Linh Phan created on Jan 28, 2016 5:23:17 PM
 */
public class FileServiceImpl implements FileService {

    public String produceFilenameWithDateTime(String fileName, String dateTimeFormat, String extension) {
        if (StringUtils.isBlank(fileName) && StringUtils.isBlank(dateTimeFormat))
            return null;

        StringBuilder result = new StringBuilder(fileName + "-" + DateTime.now().toString(DateTimeFormat.forPattern(dateTimeFormat)));
        if (StringUtils.isNotBlank(extension))
            result.append("." + extension);

        return result.toString();
    }

    public File producePDFFromImageURL(String pdfName, String imageUrl) throws MalformedURLException, IOException, DocumentException {
        if (StringUtils.isBlank(pdfName) && StringUtils.isBlank(imageUrl))
            return null;

        File pdfFile = File.createTempFile(pdfName, "");
        pdfFile.deleteOnExit();

        OutputStream pdfOutputStream = new FileOutputStream(pdfFile);

        Document document = new Document();
        PdfWriter.getInstance(document, pdfOutputStream);

        document.open();
        Image image = Image.getInstance(new URL(imageUrl));

        // scale the image fit to the PDF file
        float scaler = ((document.getPageSize().getWidth() - document.leftMargin() - document.rightMargin() - 0) / image.getWidth()) * 100;
        image.scalePercent(scaler);

        document.add(image);
        document.close();

        return pdfFile;
    }

    public String produceFilenameWithSystemNanoTime(String fileName, String extension) {
        if (StringUtils.isBlank(fileName))
            return null;

        StringBuilder result = new StringBuilder(fileName + "-" + System.nanoTime());
        if (StringUtils.isNotBlank(extension))
            result.append("." + extension);

        return result.toString();
    }
}