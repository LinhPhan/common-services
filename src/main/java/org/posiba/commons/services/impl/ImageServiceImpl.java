/**
 * Copyright 2015 Posiba, Inc. All rights reserved.
 * POSIBA, INC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF POSIBA, INC.
 * The copyright notice above does not evidence any actual or intended
 * publication of such source code.
 **/
package org.posiba.commons.services.impl;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.posiba.commons.services.ImageService;

/**
 *
 * @author Linh Phan created on Jan 29, 2016 10:24:49 AM
 */
public class ImageServiceImpl implements ImageService {

    public File produceImageFromBase64(String imageName, String imageBase64) throws IOException {
        if (StringUtils.isBlank(imageName) && StringUtils.isBlank(imageBase64))
            return null;

        File file = File.createTempFile(imageName, "");
        file.deleteOnExit();

        imageBase64 = imageBase64.substring("data:image/png;base64,".length());
        byte[] contentData = imageBase64.getBytes();
        byte[] data = Base64.decodeBase64(contentData);

        BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(file));
        stream.write(data);
        stream.close();

        return file;
    }
}
