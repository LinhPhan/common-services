/**
 * Copyright 2015 Posiba, Inc. All rights reserved.
 * POSIBA, INC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF POSIBA, INC.
 * The copyright notice above does not evidence any actual or intended
 * publication of such source code.
 **/
package org.posiba.commons.services;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;

import com.itextpdf.text.DocumentException;

/**
 *
 * @author Linh Phan created on Jan 28, 2016 5:12:31 PM
 */
public interface FileService {
    public String produceFilenameWithDateTime(String fileName, String dateTimeFormat, String extension);

    public String produceFilenameWithSystemNanoTime(String fileName, String extension);

    public File producePDFFromImageURL(String pdfName, String imageUrl) throws MalformedURLException, IOException, DocumentException;
}
