/**
 * Copyright 2015 Posiba, Inc. All rights reserved.
 * POSIBA, INC PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 * THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF POSIBA, INC.
 * The copyright notice above does not evidence any actual or intended
 * publication of such source code.
 **/
package org.posiba.commons.services;

import java.io.File;
import java.util.Map;

/**
 *
 * @author Linh Phan created on Jan 28, 2016 11:45:49 AM
 */
public abstract class EmailService {
    public abstract void send(String sender, String recipient, String subject, String content, Map<String, File> attachments);

    public abstract void send(String host, String port, final String username, final String password, String sender, String recipient, String subject, String content, Map<String, File> attachments);
}
